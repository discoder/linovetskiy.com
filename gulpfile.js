var gulp = require('gulp');
var gulpPug = require('gulp-pug');
var gulpLess = require('gulp-less');

gulp.task('compileRootPug', function(){
  return gulp.src('./app/*.pug')
        .pipe( gulpPug() )
        .pipe( gulp.dest('dist') );
});

gulp.task('compileShowcasePug', function(){
  return gulp.src('./app/showcase/*.pug')
        .pipe( gulpPug() )
        .pipe( gulp.dest('dist/showcase') );
});

gulp.task('less', function(){
  return gulp.src('./app/less/*.less')
        .pipe( gulpLess() )
        .pipe( gulp.dest('dist/styles') );
});

gulp.task('default', ['compileRootPug', 'compileShowcasePug', 'less']);
