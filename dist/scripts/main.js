// Functions
function handleMaxWidthChange(mql) {
    if (mql.matches) {
        // Mobile
        document.body.classList.add('is--mobile');
        document.body.classList.remove('is--desktop');
    } else {
        // Desktop
        document.body.classList.add('is--desktop');
        document.body.classList.remove('is--mobile');

        if ( document.body.classList.contains('is--open-menu') ) {
            document.body.classList.remove('is--open-menu');
            pgHeader.classList.remove('pg-header--fixed');
            menu.classList.remove('nav--active');
            overlay.classList.remove('overlay--active');
        }
    }
}

// Media-queries Listener
var mql = window.matchMedia("(max-width: 760px)");
mql.addListener(handleMaxWidthChange);
handleMaxWidthChange(mql);

// Main nav toggler
menuTrigger.addEventListener('click', function(event) {
    document.body.classList.toggle('is--open-menu');
    pgHeader.classList.toggle('pg-header--fixed');
    menu.classList.toggle('nav--active');
    overlay.classList.toggle('overlay--active');
});
